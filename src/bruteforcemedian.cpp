/*
 *
 * bruteforcemedian.cpp
 *
 * This file has the definition of bruteforcemedian.h
 *
 *
 * @Author:          Saujan Thapa
 * @Student number:  n9003096
 * @Unit:            CAB301
 * @version:         1.0
 * @Date:            07 May 2016
 */

#include "bruteforcemedian.h"
#include <iostream>
#include <ctime>
#include <ratio>
#include <chrono>
#include <math.h>

using namespace std;

//constructor of class bruteforcemedian
bruteforcemedian :: bruteforcemedian(int *A,int size)
{
  r_array= new int [size];
  for (int i=0;i<size;i++)
  {
    r_array[i]=A[i];
  }
  length=size;
  no_operation=0;
  time_elapsed=0.0;
  median=0;
}

//this method calculates the time taken to search the median
void bruteforcemedian :: calcualte_timespan_bruteforcemedian(void)
{
  chrono::high_resolution_clock::time_point t1=chrono::high_resolution_clock::now();
  double k=0;
  int numsmaller=0;
  int numequal=0;
  k=round(length/2.0);
  for (int i=0;i<length;i++)
  {
    numsmaller=0;
    numequal=0;
    for (int j=0;j<length;j++)
    {
      if (r_array[j] < r_array[i]){
        numsmaller += 1;
      }
      else if (r_array[j] == r_array[i]){
        numequal += 1;
      }
    }
    if( (numsmaller<k) && (k<=(numsmaller+numequal)) ){
      median = r_array[i];
      break;
    }
  }
  chrono::high_resolution_clock::time_point t2=chrono::high_resolution_clock::now();
  chrono::duration<double> time_span = chrono::duration_cast<chrono::duration<double> >(t2 - t1);
  time_elapsed=time_span.count();
}

//this method calculates the no. of basic operation to search the median
void bruteforcemedian :: calculate_no_opration_bruteforcemedian(void)
{
  double k=0;
  int numsmaller=0;
  int numequal=0;
  k=round(length/2.0);
  for (int i=0;i<length;i++)
  {
    numsmaller=0;
    numequal=0;
    for (int j=0;j<length;j++)
    {
      if (r_array[j] < r_array[i]){
        numsmaller += 1;
      }
      else if(r_array[j] == r_array[i]){
        numequal += 1;
      }
      no_operation += 1;
    }
    if( (numsmaller<k) && (k<=(numsmaller+numequal)) ){
      median = r_array[i];
      break;
    }
  }
}

//returns the size of the array
int bruteforcemedian :: size_of_array(void)
{
  return length;
}

//returns the median of the array
int bruteforcemedian :: get_median(void)
{
  return median;
}

//returns the original array
int* bruteforcemedian :: raw_array(void)
{
  return r_array;
}

//returns the time taken to search the median
double bruteforcemedian :: time_taken(void)
{
  return time_elapsed;
}

long long bruteforcemedian :: number_of_operation(void)
{
  return no_operation;
}
