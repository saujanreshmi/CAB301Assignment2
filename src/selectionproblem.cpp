/*
 *
 * selectionproblem.cpp
 *
 * This file has the definition of selectionproblem.h
 *
 *
 * @Author:          Saujan Thapa
 * @Student number:  n9003096
 * @Unit:            CAB301
 * @version:         1.0
 * @Date:            07 May 2016
 */

#include "selectionproblem.h"
#include <iostream>
#include <ctime>
#include <ratio>
#include <chrono>

using namespace std;

//constructor of class selectionproblem
selectionproblem :: selectionproblem(int *A,int size)
{
  r_array= new int[size];
  s_array= new int [size];
  for (int i=0;i<size;i++)
  {
    r_array[i]=A[i];
    s_array[i]=A[i];
  }
  median=0;
  length=size;
  time_elapsed=0.0;
  no_operation=0;
}

//this method is a part of selection problem algorithm
int selectionproblem :: Partition(int* A, int l, int h)
{
  int pivotval = A[l];
  int pivotloc = l;
  int swapper=0;
  for (int j=l+1;j<=h;j++)
  {
    if (A[j]<pivotval)
    {
      pivotloc += 1;
      //swap (A[pivotloc] and A[j])
      swapper=A[pivotloc];
      A[pivotloc]=A[j];
      A[j]=swapper;
    }
  }
  //swap (A[l] and A[pivotloc])
  swapper=A[pivotloc];
  A[pivotloc]=A[l];
  A[l]=swapper;
  return pivotloc;
}

//this method is also the part of selection problem algorithm
int selectionproblem :: Select(int* A,int l,int m, int h)
{
  int pos=Partition(A,l,h);
  if (pos == m)
  {
    return A[pos];
  }
  if (pos > m)
  {
    return Select(A,l,m,pos-1);
  }
  if (pos < m)
  {
    return Select(A,pos+1,m,h);
  }
  return 0;
}

//this method is final part of selection problem algorithm
int selectionproblem :: Median(int* A)
{
  if (length==1)
  {
    return A[0];
  }
  else
  {
    return Select(A,0,length/2,length-1);
  }
  //return 0;
}

//this method is a part of selection problem algorithm
int selectionproblem :: Partition_basic_operation(int* A, int l, int h)
{
  int pivotval = A[l];
  int pivotloc = l;
  int swapper=0;
  for (int j=l+1;j<=h;j++)
  {
    if (A[j]<pivotval)
    {
      pivotloc += 1;
      //swap (A[pivotloc] and A[j])
      swapper=A[pivotloc];
      A[pivotloc]=A[j];
      A[j]=swapper;
      no_operation += 1;
    }
  }
  //swap (A[l] and A[pivotloc])
  swapper=A[pivotloc];
  A[pivotloc]=A[l];
  A[l]=swapper;
  return pivotloc;
}

//this method is also the part of selection problem algorithm
int selectionproblem :: Select_basic_operation(int* A,int l,int m, int h)
{
  int pos=Partition_basic_operation(A,l,h);
  if (pos == m)
  {
    return A[pos];
  }
  if (pos > m)
  {
    return Select_basic_operation(A,l,m,pos-1);
  }
  if (pos < m)
  {
    return Select_basic_operation(A,pos+1,m,h);
  }
  return 0;
}

//this method is final part of selection problem algorithm
int selectionproblem :: Median_basic_operation(int* A)
{
  if (length==1)
  {
    return A[0];
  }
  else
  {
    return Select_basic_operation(A,0,length/2,length-1);
  }
  //return 0;
}

//this method calculates the time taken when searching median
void selectionproblem :: calcualte_timespan_inselection_problem(void)
{
  chrono::high_resolution_clock::time_point t1=chrono::high_resolution_clock::now();
  median=Median(s_array);
  chrono::high_resolution_clock::time_point t2=chrono::high_resolution_clock::now();
  chrono::duration<double> time_span = chrono::duration_cast<chrono::duration<double> >(t2 - t1);
  time_elapsed=time_span.count();
}

//this method calculates the no. of basic operation peformed when searching median
void selectionproblem :: calculate_no_opration_inselection_problem(void)
{
  int localmedian=Median_basic_operation(r_array);
}

//returns the size of the array
int selectionproblem :: size_of_array(void)
{
  return length;
}

//returns the median of the array
int selectionproblem :: get_median(void)
{
  return median;
}

//returns the original array
int* selectionproblem :: raw_array(void)
{
  return r_array;
}

//returns the time taken to search the median
double selectionproblem :: time_taken(void)
{
  return time_elapsed;
}

long long selectionproblem :: number_of_operation(void)
{
  return no_operation;
}
