/*
 * main.cpp
 *
 * This is the main function file that executes the bruteforcemedian and selectionproblem algorithms
 *
 * @Author:          Saujan Thapa
 * @Student number:  n9003096
 * @Unit:            CAB301
 * @version:         1.0
 * @Date:            07 May 2016
 */

/*HEADER FILES*/
//local
#include "bruteforcemedian.h"
#include "selectionproblem.h"
#include "arraygenerator.h"
//standard
using namespace std;

int main(int argc, char* argv[])
{
  int number_of_list;
  int* length_of_each_array;
  int random_manual;
  arraygenerator* ag1;
  cout <<"=======================WELCOME TO BRUTE FORCE MEDIAN & SELECTION PROBLEM ALGORITHM EVALUATOR========================"<<endl;
  cout << " => Enter the number of arrays you want to test: ";
  cin >>number_of_list;
  length_of_each_array = new int[number_of_list];
  for (int index=0;index<number_of_list;index++)
  {
    cout << " => Enter the length of array " << index+1 <<" : ";
    cin >> length_of_each_array[index];
  }
  ag1 = new arraygenerator(number_of_list,length_of_each_array);
  random_manual=ag1->input_array_option();
  ag1->create_arrays(random_manual);
  ag1->summary();
  cout<<"====================================================================================================================="<<endl;
  delete ag1;
  return 0;
}
