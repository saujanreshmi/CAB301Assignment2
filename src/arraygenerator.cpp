/*
 *
 * arraygenerator.cpp
 *
 * This file has the definition of arraygenerator.h
 *
 *
 * @Author:          Saujan Thapa
 * @Student number:  n9003096
 * @Unit:            CAB301
 * @version:         1.0
 * @Date:            07 May 2016
 */
#include "arraygenerator.h"

using namespace std;

//this method sends out the list of random array of provided length
int* arraygenerator :: list_random_value(int n)
{
 int* values=new int[n];
 for (int i=0;i<n;i++)
 {
   values[i]=rand()%R_MAX;
 }
 return values;
}

//this method sends out the list of user-defined array of provided length
int* arraygenerator :: list_userdefined_value(int n)
{
  int* values=new int[n];
  for (int i=0;i<n;i++)
  {
    cin>>values[i];
  }
  return values;
}

//constructor of the arraygenerator class
arraygenerator :: arraygenerator(int arrays, int* ind_length)
{
  how_many_arrays = arrays;
  //dyanamic initialization of arrays of type bruteforcemedian
  bfm1 = new bruteforcemedian*[arrays];
  //dyanamic initialization of arrays of type selectionproblem
  sp1 = new selectionproblem*[arrays];
  length_each_array = ind_length;
  //keeps track of basic operations of all array for the average
  total_basic_opration_brute_force_median=0;
  //keeps track of execution time of all array for the average
  total_time_taken_brute_force_median=0.0;
  //keeps track of basic operations of all array for the average
  total_basic_opration_selection_problem=0;
  //keeps track of execution time of all array for the average
  total_time_taken_selection_problem=0.0;
}

//destructor of the arraygenerator class that deletes the allocated space by constructor
arraygenerator :: ~arraygenerator()
{
  for (int i=0;i<how_many_arrays;i++)
  {
    delete bfm1[i];
    delete sp1[i];
  }
  delete bfm1;
  delete sp1;
}

//gives the option to select random or manul arrays and feedbacks the option
int arraygenerator :: input_array_option(void)
{
  int choice=0;
  cout<<"     -------------------"<<endl;
  cout<<"       Select Option    "<<endl;
  cout<<"     -------------------"<<endl;
  cout<<"       1. Random arrays"<<endl;
  cout<<"       2. Manual arrays"<<endl;
  cout<<"     -------------------"<<endl;
  cout<<"       Enter the value(1-2): ";
  do{
    cin>>choice;
    if(choice!=1&&choice!=2)
    {
      cout<<"Invalid input! Please enter again: ";
    }
  }while(choice!=1&&choice!=2);
   return choice;
}

//initializes the created array of objects
void arraygenerator :: create_arrays(int option)
{
  int* temp;
  for (int i=0;i<how_many_arrays;i++){
    //choices between random or manual
    switch(option){
      case 1:
        temp=list_random_value(length_each_array[i]);
        bfm1[i]= new bruteforcemedian(temp,length_each_array[i]);
        sp1[i]= new selectionproblem(temp,length_each_array[i]);
        break;
      case 2:
        cout<<"Enter the array "<<i+1<<" of length "<<length_each_array[i]<<" :"<<endl;
        temp=list_userdefined_value(length_each_array[i]);
        bfm1[i]= new bruteforcemedian(temp,length_each_array[i]);
        sp1[i]= new selectionproblem(temp,length_each_array[i]);
        cout<<"\n\n";
        break;
    }
    temp=0;
  }
    cout << " Calculating ....."<<endl;
    for (int i=0;i<how_many_arrays;i++){
      bfm1[i]->calcualte_timespan_bruteforcemedian();
      bfm1[i]->calculate_no_opration_bruteforcemedian();
      sp1[i]->calcualte_timespan_inselection_problem();
      sp1[i]->calculate_no_opration_inselection_problem();
      total_basic_opration_brute_force_median += bfm1[i]->number_of_operation();
      total_time_taken_brute_force_median += bfm1[i]->time_taken();
      total_basic_opration_selection_problem += sp1[i]->number_of_operation();
      total_time_taken_selection_problem += sp1[i]->time_taken();
    }
}

//this method feedbacks the summary of the bubblesort algorithm
void arraygenerator:: summary(void)
{
  cout <<"=================================================SUMMARY============================================================"<<endl;
  for (int i=0;i<how_many_arrays;i++)
  {
    cout<<" S.N :                            ";
    cout<<i+1<<endl;
    //.....................................//
    cout<<" Array Length :                   ";
    cout<<bfm1[i]->size_of_array()<<endl;
    //.....................................//
    cout<<" Original Array :                 ";
    for(int index=0;index<bfm1[i]->size_of_array();index++)
    {
      cout<<bfm1[i]->raw_array()[index]<<" ";
    }
    cout<<endl;
    //=====================================//
    cout<<" ----------------------------------"<<endl;
    cout<<" ------- Brute Force Median -------"<<endl;
    cout<<" Median :                    ";
    cout<<bfm1[i]->get_median()<<endl;
    //.....................................//
    cout<<" Time Taken(seconds) :       ";
    cout<<bfm1[i]->time_taken()<<endl;
    //.....................................//
    cout<<" Number of Basic Operation : ";
    cout<<bfm1[i]->number_of_operation()<<endl;
    //=====================================//
    cout<<" ----------------------------------"<<endl;
    cout<<" -------  Selection Problem -------"<<endl;
    cout<<" Median :                    ";
    cout<<sp1[i]->get_median()<<endl;
    //.....................................//
    cout<<" Time Taken(seconds) :       ";
    cout<<sp1[i]->time_taken()<<endl;
    //.....................................//
    cout<<" Number of Basic Operation : ";
    cout<<sp1[i]->number_of_operation()<<endl;
    cout<<"---------------------------------------------------------------------------------------------------------------------"<<endl;
  }
  cout<<"====================================================================================================================="<<endl;
  cout<<" Average number of basic operation for brute force median: "<<total_basic_opration_brute_force_median/how_many_arrays<<endl;
  cout<<" Average exection time for brute force median: "<<total_time_taken_brute_force_median/how_many_arrays<<" seconds"<<endl;
  cout<<"====================================================================================================================="<<endl;
  cout<<" Average number of basic operation for selection problem: "<<total_basic_opration_selection_problem/how_many_arrays<<endl;
  cout<<" Average exection time for selection problem: "<<total_time_taken_selection_problem/how_many_arrays<<" seconds"<<endl;
}
