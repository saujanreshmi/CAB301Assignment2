/*
 *
 * This is a code for testing brute force median algorithm implementation is correct.
 *
 * @Author:     Saujan Thapa
 * @Student number: n9003096
 * @Unit:            CAB301
 * @version:         1.0
 * @Date:            07 May 2016
 */

#include "bruteforcemedian.h"

using namespace std;

//global variables
int values[24][30]={{7,6,3,5,1,2,4,9,8}, //median 5   OddRandom
                {7,6,3,11,1,2,4,9,8,10},//median 6  EvenRandom
                {7,6,4,5,2,1,1,4,6}, //median 4   OddRandomDuplicate
                {1,2,2,7,8,5,3,3,5,11}, //median 3    EvenRandomDuplicate
                {4,5,6,7,8,9,10,11,12,14,15}, //median 9    OddAscending
                {3,5,6,8,10,11,13,14,16,17}, //median 10    EvenAscending
                {4,4,6,7,8,9,9,11,12,12,13,14,15},//median 9    OddAscendingDuplicate
                {4,6,7,8,9,11,11,12,12,13,14,15},//median 11    EvenAscendingDuplicate
                {16,15,14,13,10,7,5,4,3,2,1},//median 7    OddDescending
                {16,15,14,13,12,11,10,7,5,4,3,2},//median 10     EvenDescending
                {17,16,16,15,13,13,10,9,7,7,6},//median 13     OddDescendingDuplicate
                {18,17,16,16,15,13,13,10,9,7},//median 13     EvenDescendingDuplicate
                {7,49,73,58,130,72,144,78,123,109,40,165,92,42,187,103,127,129,45,121,150,170,35,19,55,65,1},//median 78    OddRandomLarge
                {7,49,73,58,130,72,144,78,123,109,40,165,92,42,187,103,127,129,45,121,150,170,35,19,55,65,1,22},//median 73   EvenRandomLarge
                {7,49,55,58,10,72,40,78,65,40,40,7,92,42,187,103,65,29,45,21,150,49,55,19,55,65,1},//median 49     OddRandomLargeDuplicate
                {7,49,55,58,100,72,40,78,65,40,40,7,92,42,187,103,65,29,45,21,150,49,55,19,55,65,1,56},//median 55     EvenRandomLargeDuplicate
                {2,4,6,7,9,12,19,24,28,35,45,48,51,59,70,96,123,127,135,145,149,152,156,164,174,182,190},//median 59      OddAscendingLarge
                {2,4,6,7,9,12,19,24,28,35,45,48,51,59,70,96,123,127,135,145,149,152,156,164,174,182,190,193,197,200},//median 70    EvenAscendingLarge
                {2,4,6,7,7,12,12,24,24,35,45,48,51,51,59,96,127,127,135,149,149,152,164,164,164,182,190},//median 51    OddAscendingLargeDuplicate
                {2,4,6,7,7,12,12,24,24,35,45,48,51,59,59,59,127,127,135,149,149,152,164,164,164,182,190,190,200,200},//median 59     EvenAscendingLargeDuplicate
                {200,190,182,174,164,156,152,149,145,135,127,123,96,70,60,59,51,48,45,35,28,24,19,12,9,7,6,4,2},//median 60     OddDescendingLarge
                {200,190,182,174,164,156,152,149,145,135,127,123,96,70,64,62,59,51,48,45,35,28,24,19,12,9,7,6,4,2},//median 62     EvenDescendingLarge
                {200,190,190,164,164,156,156,156,145,145,127,127,96,96,96,60,60,48,45,45,28,28,19,19,9,7,7,7,2},//median 96      OddDescendingLargeDuplicate
                {200,200,190,190,164,164,156,156,156,145,145,127,127,127,96,60,60,60,48,45,45,28,28,19,19,9,7,7,7,2}};//median 60     EvenDescendingLargeDuplicate

bruteforcemedian* bfmtest[24];

int sizes[24]={9,10,9,10,11,10,13,12,11,12,11,10,27,28,27,28,27,30,27,30,29,30,29,30};
int medians[24]={5,6,4,3,9,10,9,11,7,10,13,13,78,73,49,55,59,70,51,59,60,62,96,60};
void Testbruteforcemedian(bruteforcemedian* test,int median);

int main(void)
{
  cout<<endl<<"                   Testing Brute Force Median                    "<<endl;
  cout<<"================================================================="<<endl;
  cout<<"Test        Length        Actual        Expected        Pass/Fail"<<endl;
  cout<<"================================================================="<<endl;
  for (int index=0;index<24;index++)
  {
    if (index<9)
      cout<<"  ";
    else
    cout<<" ";
    cout<<index+1<<"         ";
    bfmtest[index]=new bruteforcemedian(values[index],sizes[index]);
    Testbruteforcemedian(bfmtest[index],medians[index]);
    cout<<endl;
  }
  cout<<"================================================================="<<endl;
  return 0;
}

void Testbruteforcemedian(bruteforcemedian* test,int median)
{
  test->calculate_no_opration_bruteforcemedian();
  if (test->size_of_array()<10)
  cout<<"   ";
  else
  cout<<"  ";
  cout<<test->size_of_array()<<"          ";
  if (test->get_median()<10)
  cout<<"   ";
  else
  cout<<"  ";
  cout<<test->get_median()<<"           ";
  if (median<10)
  cout<<"   ";
  else
  cout<<"  ";
  cout<<median<<"           ";
  if (test->get_median()==median)
  {
    cout<<"  Pass   ";
  }
  else
  {
    cout<<"  Fail   ";
  }
}
