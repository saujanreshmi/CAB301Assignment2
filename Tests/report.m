function g=report()
% report() 
%   This plots the data extracted from C++ program
f= fopen('report.txt');
g=fscanf(f,'%f');
fclose(f);

sizes=g(1:5:end);
operations_brute=g(2:5:end);
timetaken_brute=g(3:5:end);
operations_selection=g(4:5:end);
timetaken_selection=g(5:5:end);

figure(1);
% this plots the data for time taken vs size of array
subplot(1,2,1);
title('Brute force algorithm');
xlabel('Size of set');
ylabel('Execution time(seconds)');
hold on
scatter(sizes,timetaken_brute);
hold on;
% best line fit
coef_fit = polyfit(sizes,timetaken_brute,2);
y_fit = polyval(coef_fit,sizes);
plot(sizes,y_fit,'r');
hold off;

subplot(1,2,2);
title('Selection problem');
xlabel('Size of set');
ylabel('Execution time(seconds)');
hold on;
scatter(sizes,timetaken_selection);
hold on;
% best line fit
coef_fit = polyfit(sizes,timetaken_selection,2);
y_fit = polyval(coef_fit,sizes);
plot(sizes,y_fit,'r');
hold off;


figure(2);
%   title('Number of operation vs array size');
subplot(1,2,1);
title('Brute force algorithm');
xlabel('Size of set');
ylabel('Number of basic operations');
hold on;
scatter(sizes,operations_brute);
hold on;
coef_fit = polyfit(sizes,operations_brute,2);
y_fit = polyval(coef_fit,sizes);
plot(sizes,y_fit,'r');
hold off;


subplot(1,2,2);
title('Selection problem');
xlabel('Size of set');
ylabel('Number of basic operations');
hold on;
scatter(sizes,operations_selection);
hold on;
coef_fit = polyfit(sizes,operations_selection,2);
y_fit = polyval(coef_fit,sizes);
plot(sizes,y_fit,'r');
hold off;

end

