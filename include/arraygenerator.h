/*
 * arraygenerator.h
 *
 * This is the implementation for fetching the random or manual array to bruteforcemedian and selectionproblem algorithm
 *
 */

#ifndef _ARRAYGENERATOR_H_
#define _ARRAYGENERATOR_H_

#include "bruteforcemedian.h"
#include "selectionproblem.h"
#include <cstdlib>

#define R_MAX 200

class arraygenerator
{
  /**
   *
   * When you create an object of this class you need to pass the number of arrays you want to sort and also the corresponding length of them.
   *
   *
   * This class has getters for extracting the data stored by class bruteforcemedian and selectionproblem
   *
   * @Author:          Saujan Thapa
   * @Student number:  n9003096
   * @Unit:            CAB301
   * @version:         1.0
   * @Date:            07 May 2016
   */
 private:
   //stores the number of arrays that needs to be sorted
   int how_many_arrays;
   //stores the length of each arrays
   int* length_each_array;
   //pointers of pointers of instance of bruteforcemedian
   bruteforcemedian** bfm1;
   //pointers of pointers of instance of selectionproblem
   selectionproblem** sp1;
   //sends the n number of random elements
   int* list_random_value(int n);
   //sends the n number of user-input elements
   int* list_userdefined_value(int n);

 public:
   //initializes the data members by creating the array of objects
   arraygenerator(int arrays,int* ind_length);
   //gives the option to select random or manul arrays
   int input_array_option();
   //creates the bruteforcemedian & selectionproblem objects by passing the arrays
   //also calls the calculate_no_opration and calcualte_timespan
   void create_arrays(int option);
   //frees the memory of used by object of bruteforcemedian & selectionproblem
   ~arraygenerator();
   //displays the information regarding bruteforcemedian & selectionproblem algorithm
   void summary();
   //keeps track of basic operations of all array for the average
   long long total_basic_opration_brute_force_median;
   //keeps track of execution time of all array for the average
   double total_time_taken_brute_force_median;
   //keeps track of basic operations of all array for the average
   long long total_basic_opration_selection_problem;
   //keeps track of execution time of all array for the average
   double total_time_taken_selection_problem;
};

#endif
