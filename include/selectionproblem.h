
/*
 * selectionproblem.h
 *
 * This is the implementation of algorithm given to us for solving regarding Assignment 2.
 * This is a BERMAN AND PAUL'S SELECTION PROBLEM ALGORITHM implementation using class.
 *
 */

#ifndef _SELECTIONPROBLEM_H_
#define _SELECTIONPROBLEM_H_

#include <iostream>


class selectionproblem
{
  /**
   *
   * When you create an object of this class you need to pass the array of data you want to be sorted.
   *
   *
   * This class has getters for extracting the raw array,time taken to find the median,number of basic operations,
   * median of array and size of array.
   *
   * @Author:          Saujan Thapa
   * @Student number:  n9003096
   * @Unit:            CAB301
   * @version:         1.0
   * @Date:            07 May 2016
   */
private:
  //stores the raw array for calculating time_elapsed
  int *s_array;
  //stores the raw array for calculating no of basic operation
  int *r_array;
  //stores the median of an array
  int median;
  //store the number of total number of members in array
  int length;
  //stores the total time taken to find the median
  double time_elapsed;
  //counts the number of basic operation
  long long no_operation;
  //part of selection problem algorithm
  int Median(int* A);
  //part of selection problem algorithm
  int Select(int* A, int l, int m, int h);
  //part of selection problem algorithm
  int Partition(int* A, int l, int h);
  /**
   * This is another implementation for calculating number of basic operation
   */
  //part of selection problem algorithm
  int Median_basic_operation(int* A);
  //part of selection problem algorithm
  int Select_basic_operation(int* A, int l, int m, int h);
  //part of selection problem algorithm
  int Partition_basic_operation(int* A, int l, int h);


public:
  /**
   * initializes the member datatypes for performing Berman and Paul's selection problem algorithm to find median in an array
   */
  selectionproblem(int *A,int size);
  // calculates the time taken when searching the median
  void calcualte_timespan_inselection_problem(void);
  // calculates the number of basic operation performed when searching the median
  void calculate_no_opration_inselection_problem(void);
  //returns the size of the this array;
  int size_of_array();
  // this method extracts the median of the array.
  int get_median();
  // this mehtod extracts the original array.
  int* raw_array();
  // this method sends the total time taken
  double time_taken();
  // this method sends the total number of basic opration performed
  long long number_of_operation();

};

#endif
