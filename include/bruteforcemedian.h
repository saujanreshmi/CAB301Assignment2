
/*
 * bruteforcemedian.h
 *
 * This is the implementation of algorithm given to us for solving regarding Assignment 2.
 * This is a lEVITINS' BRUTE FORCE ALGORITHM implementation using class.
 *
 */

#ifndef _BRUTEFORCEMEDIAN_H_
#define _BRUTEFORCEMEDIAN_H_

#include <iostream>


class bruteforcemedian
{
  /**
   *
   * When you create an object of this class you need to pass the array of numbers and size of it
   *
   *
   * This class has getters for extracting the raw array,time taken to find the median, median of this.array and size of array.
   *
   * @Author:          Saujan Thapa
   * @Student number:  n9003096
   * @Unit:            CAB301
   * @version:         1.0
   * @Date:            07 May 2016
   */
private:
 //stores the raw array
 int *r_array;
 //stores the median of an array
 int median;
 //store the number of total number of members in array
 int length;
 //stores the total time taken to find the median
 double time_elapsed;
 //counts the number of basic operation performed to find the median
 long long no_operation;

public:
  /**
   * initializes the member datatypes for performing levitins' brute force algorithm to find median in an array
   */
  bruteforcemedian(int *A,int size);
  // calculates the time taken when searching the median
  void calcualte_timespan_bruteforcemedian(void);
  // calculates the number of basic operation when searching the median
  void calculate_no_opration_bruteforcemedian(void);
  //returns the size of the array
  int size_of_array(void);
  // this method extracts the median of array
  int get_median(void);
  // this mehtod extracts the original array
  int* raw_array(void);
  // this method sends the total time taken
  double time_taken(void);
  // this method extracts the total number of basic operation performed
  long long number_of_operation(void);

};

#endif
