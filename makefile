all: main

#class objects
obj/bruteforcemedian.o:	include/bruteforcemedian.h src/bruteforcemedian.cpp
	    g++ -ansi -c -o obj/bruteforcemedian.o -I include/ src/bruteforcemedian.cpp

obj/selectionproblem.o: include/selectionproblem.h src/selectionproblem.cpp
			g++ -ansi -c -o obj/selectionproblem.o -I include/ src/selectionproblem.cpp

obj/arraygenerator.o: include/arraygenerator.h include/bruteforcemedian.h src/arraygenerator.cpp
			g++ -ansi -c -o obj/arraygenerator.o -I include/ src/arraygenerator.cpp

#test objects
obj/main.o: include/bruteforcemedian.h include/selectionproblem.h include/arraygenerator.h src/main.cpp
			g++ -ansi -c -o obj/main.o -I include/ src/main.cpp

#test executable
main: obj/bruteforcemedian.o obj/selectionproblem.o obj/arraygenerator.o obj/main.o
	    g++ -o main obj/bruteforcemedian.o obj/selectionproblem.o obj/arraygenerator.o obj/main.o

#run tests
runmain : main
	./main

clean:
	rm -f obj/*.o
	rm -f main
