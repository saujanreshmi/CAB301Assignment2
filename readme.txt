###############################################################################################################
This project gives you the facility for evaluating the algorithm which finds the median in the list of numbers.

This program also gives you the option for manual or random entry of number arrays.

###############################################################################################################
How to run?
=> clone the repository using command $git clone Assignment2
=> build the program using command $make
=> run the program using command $make runmain

###############################################################################################################
How to clean the build file?
=> run the command $make clean

###############################################################################################################
How to get the figure of tests?
=> first you need to build the repo (run the make command for repo)
=> second run the make command in Tests directory
=> this will give you the 'report' file
=> run this file to extract the data in report.txt
=> and to plot the figure you will need matlab;
=> if you have matlab open the report.m and change the directory to Tests 
=> run the file report.m now;
